import * as actionType from './actionTypes'

export const isAuthenticated = () => {
    return{
        type: actionType.IS_AUTHENTICATED
    }
}

export const doAuthenticate = (user) => {
    return{
        type: actionType.DO_AUTHENTICATE,
        user: user
    }
}