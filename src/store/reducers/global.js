import * as actionType from './../actions/actionTypes';

const initialState = {
    isUserAuthenticated: false,
    userDetails: {}
};

const globalReducer = ( state=initialState, action) => {
    switch(action.type){
        case actionType.IS_AUTHENTICATED: return isUserAuthenticated(state);
        case actionType.DO_AUTHENTICATE: return doAuthenticate(state, action.user);
        default: return state;
    }
}

const isUserAuthenticated = (state) => {
    if (localStorage.getItem('userId')){
        return {...state, isUserAuthenticated: true}
    }
    return {...state, isUserAuthenticated: false};
}

const doAuthenticate = (state, userPayload) => {
    localStorage.setItem('userId', userPayload.sub.split('|')[1])
    localStorage.setItem('userData', JSON.stringify(userPayload))
    let newState = { ...state, isUserAuthenticated: true, userDetails: userPayload }
    return newState
}
export default globalReducer;