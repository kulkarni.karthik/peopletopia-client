import React, { Component } from 'react';
import { Grid, Typography, TextField, Button } from '@material-ui/core';
import { withStyles } from '@material-ui/core/styles';
import styles from './Auth.module.css';
import AuthButton from '../../components/Auth/AuthButton';
import GoogleLogo from './../../static/images/google_logo.png';
import GithubLogo from './../../static/images/github.png';
import CustomLogo from './../../static/images/oauth_logo.png';
import { withAuth0 } from '@auth0/auth0-react';
import { connect } from 'react-redux';
import { Redirect } from 'react-router-dom';
import * as actions from './../../store/actions/index';

const loginStyles = () => ({
    authActive: {
        borderBottom: '2px solid black'
    },
    btnColor: {
        background: '#1F61DC',
        color: '#fff',
        textTransform: 'capitalize',
        '&:hover': {
            backgroundColor: '#152542'
        }
    }
})

class Login extends Component {

    state = {
        loginActive: true,
        signupActive: false,
    }

    authSelectHandler = (event) => {
        let somestyle = styles.AuthTabStyles;
        let activeTab = event.target.closest('.' + somestyle);
        if (activeTab) {
            if (event.target.dataset && event.target.dataset.authTab) {
                if (event.target.dataset.authTab.includes('login')) {
                    this.setState({ loginActive: true, signupActive: false })
                } else {
                    this.setState({ loginActive: false, signupActive: true })
                }
            }
            activeTab.classList.toggle('authActive');
        }
    }

    render() {
        const { classes } = this.props;
        const loginText = "Don't remember your password ?";
        const signupText = "By Signing up, you agree to our terms of service and privacy policy";
        const { loginWithRedirect } = this.props.auth0;

        this.props.isUserAuthenticated();
        if (this.props.isUserSignedIn) {
            <Redirect to="/dashboard" />
        }

        return (
            <Grid container direction='column' alignItems='center' >
                <Grid container item direction='column' xs={12} lg={12} alignItems='center' className={styles.LoginPageHeader} >
                    <Grid item container justify='center'>
                        <img className={styles.LogoStyle} src={CustomLogo} alt='dgraph'></img>
                    </Grid>
                    <Grid item >
                        <Typography variant='h3' style={{ color: "#1F61DC" }} >peopletopia.</Typography>
                    </Grid>
                </Grid>
                <Grid container justify='space-around' alignItems='center' className={styles.AuthSelector}>
                    <Grid item lg={4} ></Grid>

                    <Grid data-auth-tab='login' xs={6} lg={2} onClick={(event) => this.authSelectHandler(event)} container justify='center' item className={this.state.loginActive ? [styles.AuthTabStyles, classes.authActive].join(' ') : styles.AuthTabStyles} >Login</Grid>

                    <Grid data-auth-tab='signup' xs={6} lg={2} onClick={(event) => this.authSelectHandler(event)} container justify='center' item className={this.state.signupActive ? [styles.AuthTabStyles, classes.authActive].join(' ') : styles.AuthTabStyles}>Sign up</Grid>

                    <Grid item lg={4}></Grid>
                </Grid>
                <Grid item lg={4} ></Grid>
                <Grid id={styles.InteractiveContainer} direction='column' xs={10} lg={4} item container justify='space-around' alignItems='center'>

                    <AuthButton altLogoText='Google'
                        imgSrc={GoogleLogo}
                        btnTitle='Sign In With Google'
                        bgColor="#fff"
                        btnTextColor="#000" />

                    <AuthButton altLogoText='Github'
                        imgSrc={GithubLogo}
                        btnTitle='Sign In With Github'
                        bgColor="#000"
                        btnTextColor="#fff" />

                    <AuthButton altLogoText='Auth0'
                        imgSrc={CustomLogo}
                        btnTitle='SSO Login with Auth0'
                        bgColor="#fff"
                        btnTextColor="#152542"
                        click={loginWithRedirect} />

                    <Typography variant='button'>or</Typography>

                    <TextField
                        placeholder="you@example.com"
                        required
                        id=""
                        name="emailid"
                        fullWidth
                        variant='outlined'
                        className={styles.inputLessPadding}
                    />

                    <TextField
                        placeholder="your password"
                        required
                        id=""
                        name="passsword"
                        fullWidth
                        variant='outlined'
                        type='password'
                        className={styles.inputLessPadding}
                    />
                </Grid>
                <Grid item lg={4} ></Grid>

                <Grid container item xs={10} lg={4} >
                    <Grid item xs={1} lg={3} ></Grid>
                    <Grid item xs={10} lg={6} style={{ textAlign: 'center' }} >
                        <Typography variant='caption'>
                            {this.state.loginActive ? loginText : signupText}
                        </Typography>
                    </Grid>
                    <Grid item xs={1} lg={3} ></Grid>
                </Grid>

                <Grid id={styles.AuthTriggerBtn} container item xs={10} lg={4} >
                    <Grid item lg={4} ></Grid>
                    <Grid item xs={12} lg={4} style={{ textAlign: 'center' }} >
                        <Button fullWidth variant="contained" className={classes.btnColor}> {this.state.loginActive ? 'Log in' : 'Sign up'} </Button>
                    </Grid>
                    <Grid item lg={4} ></Grid>
                    {/* <Button style={{position: 'fixed', bottom: '0'}} fullWidth variant="contained" color="primary"> {this.state.loginActive ? 'Login' : 'Sign up'} </Button> */}
                </Grid>

            </Grid>
        );
    }
}


const mapStateToProps = state => {
    return {
        isUserSignedIn: state.globalReducer.isUserAuthenticated,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        isUserAuthenticated: () => dispatch(actions.isAuthenticated())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(withAuth0(withStyles(loginStyles)(Login)));