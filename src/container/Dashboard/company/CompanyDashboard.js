import React, {Component} from 'react';
import styles from './Company.module.css';
import { Grid, Typography } from '@material-ui/core';
import People from './People';
import Values from '../../../components/Dashboard/company/Values';
import Department from './Department';
import OrgChart from './OrgChart';

class CompanyDashboard extends Component{
    state = {
        activeMenu: 'people'
    }

    companyPageMenuHandler = (clickedMenuItem) => {
        document.querySelectorAll('.' + styles.MenuItem).forEach(menu => menu.classList.remove(styles.ActiveMenu));
        clickedMenuItem.classList.add(styles.ActiveMenu);
        this.setState({activeMenu: clickedMenuItem.dataset.goto});
    }

    render(){
        const dgraphLogoSrc = `https://driftt.imgix.net/https%3A%2F%2Fdriftt.imgix.net%2Fhttps%253A%252F%252Fs3.amazonaws.com%252Fcustomer-api-avatars-prod%252F2045362%252F18010fdcab8097accc0c91f06e7f6ce4zv32bem4efh3%3Ffit%3Dmax%26fm%3Dpng%26h%3D200%26w%3D200%26s%3D90487ae16e7664d49a413603aad30e31?fit=max&fm=png&h=200&w=200&s=d81bda825bfd40e06ff215e166f6666b`;
        
        let currentContent = null;
        switch(this.state.activeMenu){
            case 'people':
                currentContent = <People />;
                break;
            case 'dept':
                currentContent = <Department />;
                break;
            case 'org':
                currentContent = <OrgChart />;
                break;
            case 'values':
                currentContent = <Values />;
                break;
            default:
                currentContent = <People />;
        }

        return(
            <Grid container>

                {/* dgraph logo with title */}
                <Grid item container md={12} id={styles.CompanyContainerHeader}>
                    <Grid item md={1}></Grid>
                    <Grid item container md={10}>
                        <Grid container direction='column'>
                            <Grid container item  alignItems='flex-start'>
                                <img 
                                    id={styles.DgraphLogo}
                                    alt='dgraph-logo'
                                    src={dgraphLogoSrc} />
                                <Typography style={{fontWeight: 'bold'}} variant='h4'>Dgraph Labs</Typography>
                            </Grid>
                        </Grid> 
                    </Grid>
                    <Grid item md={1}></Grid>
                </Grid>


                {/* menu items : people, dept, org chart, values */}
                <Grid item container md={12} id={styles.CompanyContainerMenu}>
                    <Grid item md={1}></Grid>
                    <Grid item container md={10}>
                        <Grid container item  alignItems='flex-start' id={styles.CompanyMenuItemContainer}>
                                <ul>
                                    <li>
                                        <Typography 
                                            className={[styles.MenuItem, styles.ActiveMenu].join(' ')}
                                            data-goto='people' 
                                            onClick={(event) => this.companyPageMenuHandler(event.target)} 
                                            variant='subtitle1'>
                                            People
                                        </Typography>
                                    </li>
                                    <li>
                                        <Typography 
                                            className={styles.MenuItem}
                                            data-goto='dept' 
                                            onClick={(event) => this.companyPageMenuHandler(event.target)} 
                                            variant='subtitle1'>
                                            Department
                                        </Typography>
                                    </li>
                                    <li>
                                        <Typography 
                                            className={styles.MenuItem}
                                            data-goto='org' 
                                            onClick={(event) => this.companyPageMenuHandler(event.target)} 
                                            variant='subtitle1'>
                                            Org Chart
                                        </Typography>
                                    </li>
                                    <li>
                                        <Typography 
                                            className={styles.MenuItem}
                                            data-goto='values' 
                                            onClick={(event) => this.companyPageMenuHandler(event.target)} 
                                            variant='subtitle1'>
                                            Values
                                        </Typography>
                                    </li>
                                </ul>  
                            </Grid>
                    </Grid>
                    <Grid item md={1}></Grid>
                </Grid>

                
                <Grid item container md={12}>
                    <Grid item md={1}></Grid>
                    <Grid item container md={10}>
                        { currentContent }
                    </Grid>
                    <Grid item md={1}></Grid>
                </Grid>

            </Grid>
        );
    }
}

export default CompanyDashboard;