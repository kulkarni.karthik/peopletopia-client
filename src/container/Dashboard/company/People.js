import React, {Component} from 'react';
import { Grid, InputBase } from '@material-ui/core';
import styles from './Company.module.css';
import SearchActionBtn from '../../../components/Dashboard/company/SearchActionBtn';
import PeopleDataTable from '../../../components/Dashboard/company/PeopleDataTable';

class People extends Component{
    state = {
        searchPeopleValue: "",
        peopleData: [
            {
                name: 'Zhenni Wu',
                department: 'Marketting',
                email: 'zhenni@dgraph.io',
                type: 'Full Time',
                startDate: '9 Dec 2019'
            },
            {
                name: 'Tejas Dinakar',
                department: 'Engineering',
                email: 'tdinakar@dgraph.io',
                type: 'Full Time',
                startDate: '1 Apr 2020'
            },
            {
                name: 'Anand Kumar Chandrashekar',
                department: 'Customer Success',
                email: 'anand@dgraph.io',
                type: 'Full Time',
                startDate: '9 Dec 2019'
            },
            {
                name: 'Atreyi Bose',
                department: 'Operations',
                email: 'atreyi@dgraph.io',
                type: 'Full Time',
                startDate: '9 Dec 2019'
            }
        ]
    }

    searchPeopleHandler = (searchedPerson) => {
        this.setState({searchPeopleValue: searchedPerson})
    }

    render(){
        return(

            <React.Fragment>
                <Grid className={styles.CompanyMenuContent} container item md={12} justify='space-between' alignItems='baseline'>
                    <InputBase
                            id={styles.CompanySearchField}
                            placeholder="Search People"
                            type='search'
                            value={this.state.searchPeopleValue}
                            onChange={(event) => this.searchPeopleHandler(event.target.value)}
                            inputProps={{ 'aria-label': 'search' }}
                        />
                    <SearchActionBtn />
                </Grid>

                <Grid className={styles.CompanyMenuContent} container item md={12} justify='space-between' alignItems='baseline'>
                    <PeopleDataTable data={this.state.peopleData} />
                </Grid>
            </React.Fragment>

            
        )
    }
}

export default People;