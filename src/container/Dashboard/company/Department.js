import React, {Component} from 'react';
import { Grid, InputBase } from '@material-ui/core';
import styles from './Company.module.css';
import SearchActionBtn from '../../../components/Dashboard/company/SearchActionBtn';
import DepartmentDataTable from '../../../components/Dashboard/company/DepartmentDataTable';

class Department extends Component{

    state = {
        searchDeptQuery: "",
        deptData: [
            {
                deptName: 'Customer Success',
                members: 3,
                deptHeads: '--'
            },
            {
                deptName: 'Engineering',
                members: 33,
                deptHeads: '--'
            },
            {
                deptName: 'Human Resources',
                members: 2,
                deptHeads: '--'
            },
            {
                deptName: 'Marketting',
                members: 3,
                deptHeads: '--'
            },
            {
                deptName: 'Operations',
                members: 3,
                deptHeads: '--'
            },
            {
                deptName: 'Project Management',
                members: 0,
                deptHeads: '--'
            },
            {
                deptName: 'Sales',
                members: 2,
                deptHeads: '--'
            }
        ]
    }

    searchDeptHandler = (searchedDept) => {
        this.setState({searchDeptQuery: searchedDept})
    }

    render(){
        return(

            <React.Fragment>
                <Grid className={styles.CompanyMenuContent} container item md={12} justify='space-between' alignItems='baseline'>
                    <InputBase
                            id={styles.CompanySearchField}
                            placeholder="Search Department"
                            type='search'
                            value={this.state.searchDeptQuery}
                            onChange={(event) => this.searchDeptHandler(event.target.value)}
                            inputProps={{ 'aria-label': 'search' }}
                        />
                    <SearchActionBtn />
                </Grid>

                <Grid className={styles.CompanyMenuContent} container item md={12} justify='space-between' alignItems='baseline'>
                    <DepartmentDataTable data={this.state.deptData} />
                </Grid>
            </React.Fragment>

            
        )
    }
}

export default Department;