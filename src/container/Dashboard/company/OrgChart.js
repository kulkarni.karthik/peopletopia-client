import React, { Component } from 'react';
import OrganizationChart from "@dabeng/react-orgchart";

class OrgChart extends Component {

    orgHierarchy = {
        id: 'n1',
        name: 'Manish Jain',
        title: 'Chief Executive Officer',
        children: [
            {
                id: 'n2',
                name: 'Atreyi Bose',
                title: 'Director Of india, Operations'
            },
            {
                id: 'n3',
                name: 'Tejas Dinakar',
                title: 'Head of India Engineering'
            },
            {
                id: 'n4',
                name: 'Zhenni Wu',
                title: 'Developer Marketting',
                children: [
                    {
                        id: 'n5',
                        name: 'Michael Compton',
                        title: 'GraphQL lead',
                    },
                    {
                        id: 'n6',
                        name: 'Ahmed El Bannan',
                        title: 'Fullstack Engineer',
                    },
                    {
                        id: 'n7',
                        name: 'Evelyn Mietzner',
                        title: 'Marketting Analytics',
                    },
                    {
                        id: 'n8',
                        name: 'Marisa Chan',
                        title: 'Designer',
                    }
                ]
            },
            {
                id: 'n9',
                name: 'Balaji V V',
                title: 'Senior Engineering Manager'
            },
            {
                id: 'n10',
                name: 'Daniel Mai',
                title: 'Devops Partner Engineer'
            },
            {
                id: 'n11',
                name: 'Vijay Ratnam',
                title: 'VP, Head of India Engineering'
            }
        ]
    }

    render() {
        return (
            <OrganizationChart datasource={this.orgHierarchy} />
        );
    }
}

export default OrgChart;