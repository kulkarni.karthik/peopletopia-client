import React, { Component } from 'react';
import { Grid, InputBase, Typography } from '@material-ui/core';
import styles from './dashboard.module.css';
import Profile from './profile/Profile';
import ProfileNotification from './profile/ProfileNotification';
import CompanyDashboard from './company/CompanyDashboard';
import { Redirect, Link } from 'react-router-dom';
import * as actions from './../../store/actions/index';
import { connect } from 'react-redux';

class Dashboard extends Component {
    state = {
        profileActive: true,
        updatesPresent: true
    }

    menuTabHandler = (clickedMenuTab) => {
        if (clickedMenuTab.dataset.goto && clickedMenuTab.dataset.goto === 'profile') {
            this.setState({ profileActive: true });
        } else {
            this.setState({ profileActive: false });
        }
        document.querySelectorAll('.' + styles.MainMenuTab).forEach(menuItem => menuItem.classList.remove(styles.activeMenuTab));
        clickedMenuTab.classList.add(styles.activeMenuTab)
    }

    render() {
        this.props.isUserAuthenticated();
        if (!this.props.isUserSignedIn) {
            return (<Redirect to="/" />);
        }
        let userData = JSON.parse(localStorage.getItem('userData'));
        let menuClasses = [styles.MainMenuTab];

        return (
            <Grid container direction='column' justify='space-around' alignItems='center'>

                {/* row-1 containing tab menu and search bar */}
                <Grid item container md={12} id={styles.HeaderContainer}>
                    <Grid item md={1}></Grid>
                    <Grid item container md={10} justify='space-between'>
                        <Grid item container md={6} alignItems='baseline'>
                            <Grid item md={6}>
                                <Typography id={styles.HeaderCompanyName} variant='h6'>peopletopia.</Typography>
                            </Grid>
                            <Grid item md={3}>
                                <Typography
                                    className={[...menuClasses, styles.activeMenuTab].join(' ')}
                                    data-goto='profile'
                                    onClick={(event) => this.menuTabHandler(event.target)}
                                    variant='subtitle1'>
                                    My Profile
                                </Typography>
                            </Grid>
                            <Grid item md={3}>
                                <Typography
                                    className={[...menuClasses].join(' ')}
                                    data-goto='company'
                                    onClick={(event) => this.menuTabHandler(event.target)}
                                    variant='subtitle1'>
                                    Company
                                </Typography>
                            </Grid>
                        </Grid>
                        <Grid item container md={6} justify='flex-end' alignItems='baseline'>
                            <Link 
                                style={{marginRight: '1rem', textDecoration: 'none', color: '#1F61DC'}} 
                                to="/logout">
                                    Logout 
                            </Link>
                            <InputBase
                                type='search'
                                id={styles.headerSearch}
                                placeholder="Search Peopletopia"
                                inputProps={{ 'aria-label': 'search' }}
                            />
                        </Grid>
                    </Grid>
                    <Grid item md={1}></Grid>
                </Grid>

                {/* row-2 : contains wish and notifications, this is conditional */}
                {(this.state.updatesPresent && this.state.profileActive) ? <ProfileNotification userName={userData.given_name} /> : null}

                {/* row-3 : profile page, further profile page contains 'overview' & 'status update'  */}
                {this.state.profileActive ? <Profile /> : <CompanyDashboard />}

            </Grid>
        );
    }
}

const mapStateToProps = state => {
    return {
        isUserSignedIn: state.globalReducer.isUserAuthenticated,
    }
}

const mapDispatchToProps = dispatch => {
    return {
        isUserAuthenticated: () => dispatch(actions.isAuthenticated())
    }
}

export default connect(mapStateToProps, mapDispatchToProps)(Dashboard);