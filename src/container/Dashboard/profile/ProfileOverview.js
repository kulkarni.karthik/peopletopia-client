import React, { Component } from 'react';
import { Grid, Typography } from '@material-ui/core';
import styles from './Profile.module.css';
import OverviewData from '../../../components/Dashboard/profile/OverviewData';
import StatusUpdate from './StatusUpdates';

class ProfileOverview extends Component {
    state = {
        overviewActive: true
    }

    setMenuTab = (clickedTab) => {
        document.querySelectorAll('.' + styles.ProfileMenuTabOptions).forEach(node => node.classList.remove(styles.ActiveTab));
        clickedTab.classList.add(styles.ActiveTab);
        if (clickedTab.dataset.activeTab === 'overview') {
            this.setState({ overviewActive: true })
        } else {
            this.setState({ overviewActive: false })
        }
    }

    render() {
        let menuTabClasses = [styles.ProfileMenuTabOptions];
        let activeTab = [...menuTabClasses, styles.ActiveTab];
        let userdata = JSON.parse(localStorage.getItem('userData'));

        return (
            <Grid item container >
                {/* menu tabs */}
                <Grid id={styles.ProfileMenuTab} item container>
                    <Typography
                        data-active-tab='overview'
                        onClick={(event) => this.setMenuTab(event.target)}
                        className={activeTab.join(' ')}
                        variant='subtitle1'>
                        Overview
                    </Typography>
                    <Typography
                        data-active-tab='status'
                        onClick={(event) => this.setMenuTab(event.target)}
                        className={menuTabClasses.join(' ')}
                        variant='subtitle1'>
                        Status Updates
                    </Typography>
                </Grid>

                {/* current menu content - overview / updates */}
                <Grid container item >
                    {this.state.overviewActive ? <OverviewData userData={userdata} /> : <StatusUpdate />}
                </Grid>
            </Grid>
        );
    }
}

export default ProfileOverview;