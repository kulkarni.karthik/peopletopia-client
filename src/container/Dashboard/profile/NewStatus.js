import React, { Component } from 'react';
import { Grid, Typography, Button } from '@material-ui/core';
import styles from './Profile.module.css';
import CloseIcon from '@material-ui/icons/Close';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';

export const dashboardStyles = () => ({
    btnColor: {
        background: '#1F61DC',
        color: '#fff',
        textTransform: 'capitalize',
        '&:hover': {
            backgroundColor: '#152542'
        }
    },
    ModalHeader: {
        fontSize: '1.3rem',
        color: '#1F61DC',
        fontWeight: '500'
    }
});

class NewStatus extends Component {
    state = {
        newStatusMsg: ""
    }

    addNewStatus = (event) => {
        this.setState({ newStatusMsg: event.target.value });
    }

    render() {
        const {classes} = this.props;

        return (
            <Grid direction='column' container id={styles.StatusContainer}>
                <Grid item container justify='space-between' alignItems='baseline' id={styles.ModalHeader} >
                    <Grid className={classes.ModalHeader} item>New status</Grid>
                    <Grid item><CloseIcon style={{cursor: 'pointer'}} onClick={this.props.onCloseClick} /></Grid>
                </Grid>
                <Grid item container direction='column' id={styles.ModalContent}>
                    <Typography
                        id={styles.WhatsYourUpdate}
                        variant='subtitle1'
                        component='p' >
                        What's your status update ?
                    </Typography>
                    <TextField
                        onChange={(event) => this.addNewStatus(event)}
                        id="outlined-multiline-static"
                        label="Your new status..."
                        multiline
                        value={this.state.newStatusMsg}
                        rows={4}
                        variant="outlined"
                    />
                </Grid>
                <Grid container justify='flex-end' id={styles.AddNewStatusBtn}>
                    <Button variant="contained" className={classes.btnColor}>Add new status</Button>
                </Grid>
            </Grid>
        );
    }
}

export default withStyles(dashboardStyles)(NewStatus);