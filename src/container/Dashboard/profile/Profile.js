import React, { Component } from 'react';
import { Grid } from '@material-ui/core';
import PeerCard from '../../../components/Dashboard/profile/PeerCard';
import ProfilePreviewCard from '../../../components/Dashboard/profile/ProfilePreviewCard';
import styles from './.././dashboard.module.css';
import ProfileOverview from './ProfileOverview';
import { Redirect } from 'react-router-dom';

class Profile extends Component {
    state = {
        reportsToMe: [
            {
                name: "Akash Jain",
                position: 'Full stack Engineer'
            },
            {
                name: "Paul korzhyk",
                position: 'Full stack Engineer'
            },
            {
                name: "Phani Kumar",
                position: 'Site Reliability Engineer'
            },
            {
                name: "Neeraj Bhattan",
                position: 'Distributed Systems Engineer'
            },
            {
                name: "Arpan Gupta",
                position: 'Software Engineer'
            },
            {
                name: "Thyagaraj T",
                position: 'Frontend Engineer'
            }
        ]
    }

    render() {
        let userData = JSON.parse(localStorage.getItem('userData'));

        if(this.props.isUserAuthenticated){
            <Redirect to="/dashboard" />
        }

        return (
            <Grid item container md={12} id={styles.ProfileContainer}>
                <Grid item md={1}></Grid>
                <Grid item container md={10}>
                    <Grid item container md={3} >
                        <ProfilePreviewCard
                            title={userData.name}
                            position='Head of India Engineering'
                            department='Engineering' />
                        {/* whom am i reporting to ? */}
                        <h4 style={{ margin: '0' }}>Manager</h4>
                        <PeerCard
                            peerName='Manish Jain'
                            peerPosition='chief executive officer'
                        />
                        {/* who reports to me ? */}
                        <h4 style={{ margin: '0' }}>Reports</h4>
                        {
                            this.state.reportsToMe.map(peer => <PeerCard
                                key={peer.name.replace(/\s/g,'')}
                                peerName={peer.name}
                                peerPosition={peer.position}
                            />)
                        }
                    </Grid>
                    <Grid item container md={9} direction='column' >
                        <ProfileOverview />
                    </Grid>
                </Grid>
                <Grid item md={1}></Grid>
            </Grid>
        );
    }
}

export default Profile;