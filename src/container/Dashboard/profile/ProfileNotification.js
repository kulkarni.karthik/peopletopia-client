import React, { Component } from 'react';
import { Grid, Typography, Button, Modal } from '@material-ui/core';
import styles from './.././dashboard.module.css';
import AddIcon from '@material-ui/icons/Add';
import NotificationCard from '../../../components/Dashboard/NotificationCard';
import NewStatus from './NewStatus';
import { withStyles } from '@material-ui/core/styles';

const dashboardStyles = () => ({
    btnColor: {
        background: '#1F61DC',
        color: '#fff',
        textTransform: 'capitalize',
        '&:hover': {
            backgroundColor: '#152542'
        }
    }
})


class ProfileNotification extends Component {

    state = {
        isModalOpen: false
    }

    getTimeToGreet = () => {
        let curHr = new Date().getHours()
        if (curHr < 12) {
            return 'Morning';
        } else if (curHr < 18) {
            return 'Noon';
        } else {
            return 'Evening';
        }
    }

    showModal = () => {
        this.setState({ isModalOpen: true });
    };

    hideModal = () => {
        this.setState({ isModalOpen: false });
    };

    render() {
        const {classes} = this.props;

        return (
            <Grid item container md={12} id={styles.NotificationContainer}>
                <Grid item md={1}></Grid>
                <Grid item container md={10} justify='space-between'>
                    <Grid item container direction='column' justify='flex-start' alignItems='flex-start' md={3}>
                        <Typography id={styles.Greet} variant='h5'>{this.getTimeToGreet()}, {this.props.userName}</Typography>
                        <Button
                            onClick={this.showModal}
                            variant="contained"
                            className={classes.btnColor}
                            startIcon={<AddIcon />}>
                            update status
                        </Button>
                        <Modal
                            style={{width: '65%', top: '15%', left: '15%'}}
                            open={this.state.isModalOpen}
                            onClose={this.hideModal}
                            aria-labelledby="simple-modal-title"
                            aria-describedby="simple-modal-description">
                            {
                                <div>
                                    <NewStatus onCloseClick={this.hideModal} />
                                </div>
                            }
                        </Modal>
                    </Grid>
                    <Grid item container md={9} direction='column'>
                        <Grid container item md={12} justify='space-between' alignItems='baseline'>
                            <Typography variant='subtitle1'>Recent Updates</Typography>
                            <p>See All</p>
                        </Grid>
                        <Grid container item md={12}>
                            <NotificationCard
                                content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip."
                            />
                            <NotificationCard
                                content="Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip."
                            />
                        </Grid>
                    </Grid>
                </Grid>
                <Grid item md={1}></Grid>
            </Grid>
        );
    }
}

export default withStyles(dashboardStyles)(ProfileNotification);