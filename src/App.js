import './App.css';
import React, { Component, Suspense } from 'react';
import { BrowserRouter, Switch, Route, withRouter, Redirect } from 'react-router-dom';
import Auth from './container/Auth/Auth';
import Dashboard from './container/Dashboard/Dashboard';
import { Container } from '@material-ui/core';
import CircularProgress from '@material-ui/core/CircularProgress';
import { connect } from 'react-redux';
import { withAuth0 } from '@auth0/auth0-react';
import * as actions from './store/actions/index';

const Logout = React.lazy(() => import('./components/Auth/Logout'))
// const Dashboard = React.lazy(() => import('./container/Dashboard/Dashboard'))
// const Auth = React.lazy(() => import('./container/Auth/Auth'))

class App extends Component {
  render() {
    const { isLoading, isAuthenticated, user } = this.props.auth0;
    
    let routes = (
      <Switch>
        <Route
          path="/"
          exact
          render={
            () => <Suspense fallback={<CircularProgress />} ><Auth /></Suspense>
          }>
        </Route>
        <Redirect to="/" />
      </Switch>
    )

    if (isAuthenticated){ // auth0 authentication
      this.props.doAuthenticate(user);
    }
    this.props.isUserAuthenticated();

    if (this.props.isUserSignedIn) {
      routes = (
        <Switch>
          <Route
            path="/logout"
            exact
            render={
              () => <Suspense fallback={<CircularProgress />} ><Logout /></Suspense>
            }>
          </Route>

          <Route
            path="/dashboard"
            exact
            render={
              () => <Suspense fallback={<CircularProgress />} ><Dashboard /></Suspense>
            }>
          </Route>

          <Redirect to="/dashboard" />
        </Switch>
      )
    }

    return (
      <BrowserRouter>
        <Container style={{ padding: 0, margin: 0 }} maxWidth='xl'>
          { isLoading ? <CircularProgress /> : routes}
        </Container>
      </BrowserRouter>
    );
  }
}


const mapStateToProps = state => {
  return{
    isUserSignedIn: state.globalReducer.isUserAuthenticated,
  }
}

const mapDispatchToProps = dispatch => {
  return {
    doAuthenticate: (user) => dispatch(actions.doAuthenticate(user)),
    isUserAuthenticated: () => dispatch(actions.isAuthenticated())
  }
}

export default withAuth0(withRouter(connect(mapStateToProps, mapDispatchToProps)(App)));

