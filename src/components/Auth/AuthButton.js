import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import styles from './Auth.module.css';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles({
    root: {
        textTransform: 'capitalize'
    }
});

const AuthButton = (props) => {
    const classes = useStyles();
    return (
        <Grid onClick={props.click} item xs={12} className={styles.CustomLogoBtn} style={{ background: props.bgColor }} container justify='flex-start' alignItems='center'>
            <img alt={props.altLogoText} src={props.imgSrc} />
            <div>
                <Typography className={classes.root} variant='subtitle1' style={{ color: props.btnTextColor }}>
                    {props.btnTitle}
                </Typography>
            </div>
        </Grid>
    )
}

export default AuthButton;