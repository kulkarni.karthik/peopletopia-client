import React from 'react';
import { Grid, Typography, Container, TextField, Button } from '@material-ui/core';
import styles from './Auth.module.css';
import loginStyles from './../../container/Auth/Auth.module.css';
import { withStyles } from '@material-ui/core/styles';
import GoogleLogo from './../../static/images/google_logo.png';
import AuthButton from './AuthButton';

const logoutStyles = {
    logoutHeader: {
        color: '#1F61DC',
        marginBottom: '2rem',
        fontWeight: '500'
    },
    signBack: {
        fontWeight: 'bold',
        marginBottom: '0.5rem'
    },
    webAddr: {
        fontSize: '1rem',
        marginBottom: '1rem',
        color: 'grey'
    },
    TxtFieldBorder: {
        background: '#fff',
        border: '2px solid grey',
        borderRadius: '5px'
    },
    InteractiveContainer: {
        minHeight: '15rem'
    },
    ForgotPwdLink: {
        marginTop: '1rem'
    },
    PrivacyPolicy: {
        position: 'absolute',
        bottom: '2%'
    },
    btnColor: {
        background: '#1F61DC',
        color: '#fff',
        textTransform: 'capitalize',
        fontSize: '1rem',
        '&:hover': {
            backgroundColor: '#152542'
        }
    }
};

const Logout = (props) => {
    const { classes } = props;
    localStorage.removeItem('userId');
    localStorage.removeItem('userData');
    return (
        <Container maxWidth='xl' style={{ background: '#f1f1f1', height: '100vh' }}>
            <Grid container direction='column' alignItems='center' id={styles.LogoutContainer}>
                <Typography className={classes.logoutHeader} gutterBottom variant='h4'>peopletopia.</Typography>
                <Typography className={classes.signBack} variant='h5'>Sign back into Dgraph Labs</Typography>
                <Typography className={classes.webAddr} variant='caption'>dgraphlabs.peopletopia.com</Typography>

                <Grid className={classes.InteractiveContainer} direction='column' xs={10} lg={4} item container justify='space-around' alignItems='center'>
                    <AuthButton altLogoText='Google'
                        imgSrc={GoogleLogo}
                        btnTitle='Sign In With Google'
                        bgColor="#fff"
                        btnTextColor="#000" />
                    <Typography variant='button'>or</Typography>

                    <TextField
                        placeholder="company@peopletopia.com"
                        required
                        id=""
                        name="emailid"
                        fullWidth
                        color="primary"
                        variant='outlined'
                        className={[loginStyles.inputLessPadding, classes.TxtFieldBorder].join(' ')}
                    />

                    <TextField
                        placeholder="Password"
                        required
                        id=""
                        name="passsword"
                        fullWidth
                        variant='outlined'
                        type='password'
                        className={[loginStyles.inputLessPadding, classes.TxtFieldBorder].join(' ')}
                    />


                </Grid>
                <Grid id={loginStyles.AuthTriggerBtn} container item xs={10} lg={4} >
                    <Button fullWidth variant="contained" className={classes.btnColor}> Sign in with your email </Button>
                </Grid>
                <Typography className={classes.ForgotPwdLink} variant='caption'>Forgot your password ?</Typography>
                <Typography className={classes.PrivacyPolicy} variant='caption'>Privacy & Policy</Typography>
            </Grid>

        </Container>
    );
}
export default withStyles(logoutStyles)(Logout);