import React from 'react';
import { makeStyles } from '@material-ui/core/styles';
import Card from '@material-ui/core/Card';
import CardHeader from '@material-ui/core/CardHeader';
import CardContent from '@material-ui/core/CardContent';
import Avatar from '@material-ui/core/Avatar';
import Typography from '@material-ui/core/Typography';

const useStyles = makeStyles(() => ({
  root: {
    maxWidth: 'inherit',
    marginBottom: '1.5rem'
  },
  avatar: {
    backgroundColor: 'grey',
  },
}));

const NotificationCard =(props) => {
  const classes = useStyles();

  return (
    <Card className={classes.root}>
      <CardHeader
        avatar={
          <Avatar aria-label="recipe" className={classes.avatar}>
            T
          </Avatar>
        }
        title="Tejas Dinakar"
        subheader="Nov 2"
      />
      <CardContent>
        <Typography variant="body2" color="textSecondary" component="p">
            {props.content}
        </Typography>
      </CardContent>
    </Card>
  );
}


export default NotificationCard;