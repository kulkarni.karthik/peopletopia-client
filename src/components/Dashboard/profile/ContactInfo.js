import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import styles from './Profile.module.css';

const ContactInfo = (props) => {
    return(
        <Grid container item id={styles.Pinfo}>
        
            <Typography 
                variant='subtitle1' 
                className={styles.CardTitle}>
                Contact Info
            </Typography>
            
            <Grid container item md={12} alignItems='baseline' className={styles.InfoRow} >
                <Grid item md={2} className={styles.InfoFieldName}>Email</Grid>
                <Grid item md={10}>
                    <Typography 
                        className={styles.PInfoDetails} 
                        component='p' 
                        variant='subtitle1'>
                        {props.email}
                    </Typography>
                </Grid>
            </Grid>
            <Grid container item md={12} alignItems='baseline'>
                <Grid item md={2} className={styles.InfoFieldName}>Phone</Grid>
                <Grid item md={10}>
                    <Typography 
                        className={styles.PInfoDetails} 
                        component='p' 
                        variant='subtitle1'>
                        {props.phone}
                    </Typography>
                </Grid>
            </Grid>

        </Grid>
    )
}
export default ContactInfo;