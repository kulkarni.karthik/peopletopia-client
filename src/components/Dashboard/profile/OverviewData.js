import React from 'react';
import ProfileInfo from './ProfileInfo';
import ContactInfo from './ContactInfo';

const OverviewData = (props) => {
    return(
            <React.Fragment>
                <ProfileInfo 
                    location='Bangalore, India'
                    university='illinois institute of technology'
                    githubHandle={props.userData ? 'github/'+props.userData.given_name : 'tejas@dgraph.io'}
                    twitterHandle={props.userData ? '@'+props.userData.given_name : 'tejas@dgraph.io'}
                />
                <ContactInfo
                    email={props.userData ? props.userData.email : 'tejas@dgraph.io'}
                    phone='+91-987-654-3210' />
            </React.Fragment>
    );
}

export default OverviewData;