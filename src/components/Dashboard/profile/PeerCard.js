import React from 'react';
import { Grid, Avatar, Typography } from '@material-ui/core';
import styles from './Profile.module.css';

const PeerCard = (props) => {
    return(
        <Grid container item justify='flex-start' alignItems='center' className={styles.PeerCard}>
            <Grid item md={2} >
                <Avatar alt="Remy Sharp" src="https://picsum.photos/150/150" />
            </Grid>
            <Grid container item md={10}>
                <Grid item md={12}>
                    <Typography 
                        id={styles.PeerName}
                        variant='subtitle1'
                        component='p' >
                        {props.peerName}
                    </Typography>
                </Grid>
                <Grid item md={12}>
                    <Typography 
                        id={styles.PeerPosition}
                        variant='subtitle1'
                        component='p' >
                        {props.peerPosition}
                    </Typography>
                </Grid>
            </Grid>
        </Grid>
    );
}

export default PeerCard;