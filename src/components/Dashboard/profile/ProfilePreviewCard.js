import React from 'react';
import { Grid, Avatar, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import styles from './Profile.module.css';

const useStyles = makeStyles((theme) => ({
    large: {
        width: theme.spacing(30),
        height: theme.spacing(30),
    },
}));


const ProfilePreviewCard = (props) => {
    const classes = useStyles();
    let profilePic = JSON.parse(localStorage.getItem('userData')).picture;
    return (
        <Grid direction='column' container item xs={12} md={11} spacing={1} id={styles.PicCard}>
            {/* <Avatar alt="Remy Sharp" src="https://picsum.photos/300/300" className={classes.large} id={styles.Pic} /> */}
            <Avatar alt="Remy Sharp" src={profilePic} className={classes.large} id={styles.Pic} />
            <Typography id={styles.ProfileName} variant='h6' >{props.title}</Typography>
            <Typography id={styles.Position} variant='h6' >{props.position}</Typography>
            <Typography id={styles.Department} variant='subtitle1' >{props.department}</Typography>
        </Grid>
    );
}

export default ProfilePreviewCard;