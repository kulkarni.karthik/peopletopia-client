import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import styles from './Profile.module.css';

const ProfileInfo = (props) => {
    return(
        <Grid container item id={styles.Pinfo}>
        
            <Typography 
                variant='subtitle1' 
                className={styles.CardTitle}>
                Profile
            </Typography>
            
            <Grid container item md={12} alignItems='baseline' className={styles.InfoRow} >
                <Grid item md={2} className={styles.InfoFieldName}>Location</Grid>
                <Grid item md={10}>
                    <Typography 
                        className={styles.PInfoDetails} 
                        component='p' 
                        variant='subtitle1'>
                        {props.location}
                    </Typography>
                </Grid>
            </Grid>
            <Grid container item md={12} alignItems='baseline' className={styles.InfoRow} >
                <Grid item md={2} className={styles.InfoFieldName}>University</Grid>
                <Grid item md={10}>
                    <Typography 
                        className={styles.PInfoDetails} 
                        component='p' 
                        variant='subtitle1'>
                        {props.university}
                    </Typography>
                </Grid>
            </Grid>
            <Grid container item md={12} alignItems='baseline' className={styles.InfoRow} >
                <Grid item md={2} className={styles.InfoFieldName}>Github</Grid>
                <Grid item md={10}>
                    <Typography 
                        className={styles.PInfoDetails} 
                        component='p' 
                        variant='subtitle1'>
                        {props.githubHandle}
                    </Typography>
                </Grid>
            </Grid>
            <Grid container item md={12} alignItems='baseline'>
                <Grid item md={2} className={styles.InfoFieldName}>Twitter</Grid>
                <Grid item md={10}>
                    <Typography 
                        className={styles.PInfoDetails} 
                        component='p' 
                        variant='subtitle1'>
                        {props.twitterHandle}
                    </Typography>
                </Grid>
            </Grid>

        </Grid>
    )
}
export default ProfileInfo;