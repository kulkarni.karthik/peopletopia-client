import React from 'react';
import { Paper, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import styles from './Company.module.css';

const DepartmentDataTable = (props) => {
    return (
        <TableContainer component={Paper}>
            <Table style={{ minWidth: '650px' }} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell className={styles.TableHeaderTitle} align="left">Name</TableCell>
                        <TableCell className={styles.TableHeaderTitle} align="left">Members #</TableCell>
                        <TableCell className={styles.TableHeaderTitle} align="left">Department Heads</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.data.map((dept) => (
                        <TableRow key={dept.name}>
                            <TableCell component="th" scope="row">
                                {dept.deptName}
                            </TableCell>
                            <TableCell align="left">
                                {dept.members}
                            </TableCell>
                            <TableCell align="left">{dept.deptHeads}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}

export default DepartmentDataTable;