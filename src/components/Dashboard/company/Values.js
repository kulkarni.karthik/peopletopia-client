import React from 'react';
import { Grid, Typography } from '@material-ui/core';
import styles from './Company.module.css';

const Values = () => {
    return(
        <Grid container item md={8} justify='space-between' alignItems='baseline'>
            <ol id={styles.ValueList}>
                <li>
                    <Typography className={styles.ValueTitle} variant='h6' >Be Open source</Typography>
                    <Typography className={styles.ValueDescription} variant='subtitle1' >
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </Typography>
                </li>
                <li>
                    <Typography className={styles.ValueTitle} variant='h6' >Create an incrediable user experience</Typography>
                    <Typography className={styles.ValueDescription} variant='subtitle1' >
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </Typography>
                </li>
                <li>
                    <Typography className={styles.ValueTitle} variant='h6' >Simplify obsessively. Deliver quickly</Typography>
                    <Typography className={styles.ValueDescription} variant='subtitle1' >
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </Typography>
                </li>
                <li>
                    <Typography className={styles.ValueTitle} variant='h6' >Collaborate and Iterate. Experiment and Evolve.</Typography>
                    <Typography className={styles.ValueDescription} variant='subtitle1' >
                        Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.
                    </Typography>
                </li>
            </ol>
        </Grid>
    );
}

export default Values;