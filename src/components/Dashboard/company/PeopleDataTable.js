import React from 'react';
import { Grid, Paper, Avatar, Table, TableBody, TableCell, TableContainer, TableHead, TableRow } from '@material-ui/core';
import styles from './Company.module.css';

const PeopleDataTable = (props) => {
    return (
        <TableContainer component={Paper}>
            <Table style={{ minWidth: '650px' }} aria-label="simple table">
                <TableHead>
                    <TableRow>
                        <TableCell className={styles.TableHeaderTitle} align="left">Name</TableCell>
                        <TableCell className={styles.TableHeaderTitle} align="left">Department</TableCell>
                        <TableCell className={styles.TableHeaderTitle} align="left">Email</TableCell>
                        <TableCell className={styles.TableHeaderTitle} align="left">Type</TableCell>
                        <TableCell className={styles.TableHeaderTitle} align="left">Start Date</TableCell>
                    </TableRow>
                </TableHead>
                <TableBody>
                    {props.data.map((person) => (
                        <TableRow key={person.name}>
                            <TableCell component="th" scope="row">
                                <Grid container alignItems='center' >
                                    <Avatar alt="dgraph" src="https://picsum.photos/150/150" className={styles.MarginRight1Rem}/>
                                    {person.name}
                                </Grid>
                            </TableCell>
                            <TableCell align="left">
                                <Grid container alignItems='center' >
                                    <span className={[styles.Dot, styles.MarginRight1Rem].join(' ')}></span>
                                    {person.department}
                                </Grid>
                            </TableCell>
                            <TableCell align="left">{person.email}</TableCell>
                            <TableCell align="left">{person.type}</TableCell>
                            <TableCell align="left">{person.startDate}</TableCell>
                        </TableRow>
                    ))}
                </TableBody>
            </Table>
        </TableContainer>
    );
}

export default PeopleDataTable;